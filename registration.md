---
title: Registration | WPAFW
permalink: "/registration"
layout: page
banner: 🗳 Registration (2022)
banner-color: blue
nav: true
nav-title: Registration
nav-order: 1
---

## Registration levels

### Attending Membership: $80 (At Door: $90)

An attending membership allows a member:
- Access to the event for its duration (Friday, Saturday, Sunday)
- Food and drink at the event
- Any publications at the event
- You also get 2 free seed tickets for the charity auction!

### Sponsor Membership: $100 (At Door: $120)

A sponsor gets the full privileges of an attending and much more:
- An awesome Sponsor ribbon
- A special, collectible WPAFW cup
- Expedited badge pickup on site
- An extra 2 seed tickets!
- Access to our Beer and Cider Bar on site (must be over 21)
- Non-alcoholic drinks from local soda company, Natrona Bottling Company! Find more info [here](https://www.natronabottling.com/).

### Super-Sponsor Membership: $200.00

We will lavish you with gifts, praise, and thanks! A Super Sponsor gets the full privileges of a Sponsor membership and much more! Privileges such as:
- An extra special and customized WPAFW gift!
- ANOTHER extra 2 seed tickets!
- A special event (and keepsake) just for Super-Sponsors with the Guests of Honor!

# WPAFW 2022 Pre-Registration 

<div class="buttons">
  [<button class="button is-link" disabled>PRE-REG IS OPEN!</button>](https://reg.wpafw.org/)
</div>

_WPAFW registration is powered by [ConCat](https://concat.app)._

---

## How to pre-register for WPAFW:

1. Go to [our preregistration site](#).
2. Register for an account.
3. Once registered, on the left hand side bar is the option: Register for WPAFW 2021.
4. Select registration level and fill out the required information.
5. After adding your registration to the cart, there is an option to add charity tickets. Click on the link if interested!

If you have any general registration questions prior to the event, please direct them to [registration@wpafw.org](mailto:registration@wpafw.org).

We also take accessibility needs seriously. If you need any accommodations, please be sure to include the information on the registration page! If you have other accessibility related questions, please direct them to [accessibility@wpafw.org](mailto:accessibility@wpafw.org).

---

#### Things To Note

While many cons can offer day passes, the nature of WPAFW (daily food and drink all provided) makes it near impossible to plan for such changes in attendance.  We're sorry for the inconvenience.

For 2022 we are enforcing an 18+ age restriction.

We only accept Visa, MasterCard, Amex, and Discover through ConCat during pre-registration. Cash and card will be accepted at the venue.

---

# Event Policies

## General Conduct Policy

The Western Pennsylvania Furry Weekend (WPAFW) is a time where people should be able to relax and enjoy themselves. Common sense and respect for others are usually sufficient to keep everybody happy. However, it is important to note a few rules that we expect everyone to abide by. Be considerate of those around you (especially those who are not in attendance of the convention). Threats, stalking, harassment, or persistently rude behavior may result in your membership being revoked.

During the event, your badge remains the property of WPAFW and must be displayed at all times. If any staff member asks to see it, please present it to them.

These policies must be followed by all attendees, and violations will result in warnings. After 3 warnings, you will lose your WPAFW membership, be asked to leave the event, and may not be welcomed back.

Complaints regarding our attendees from our hotel will be taken seriously, and any actions that may jeopardize our relationship with the hotel may also result in loss of your WPAFW membership.

#### WPAFW cannot permit any violations of local, state, or national law.

We will take severe and immediate action against anyone who brings illegal drugs to the event, engages in vandalism, or blatantly violates any law or acts in a manner that would compromise our relationship with the Community or the Allegheny County Parks Department. Additionally, alcohol not provided and served by WPAFW is prohibited. Anyone found in possession of outside alcohol will be asked to remove it from the event and issued a warning.

#### We do not allow mature subject matter to be displayed in areas where either minors (or adults who do not wish to see it) may be exposed to it.

This includes explicit artwork, but also includes overtly mature discussions, excessive displays of public affection, and use of profanity. While we respect everyone’s lifestyle choices, please remember that North Park is still open to the citizens of Allegheny County during our event. Parents may bring their children to use the playground equipment; especially if the weather is nice.

We expect that all attendees will conduct themselves appropriately. We also expect those staying in the Hotel space to conduct themselves just as they would in any other business or restaurant.

#### Minors are not permitted to attend WPAFW.

No one under 18 will be admitted to the Western PA Furry Weekend.

#### WPAFW expects attendees to dress appropriately.

Costumes and strange clothing are fine, but if you’re showing too much skin or dressed in a manner that is overtly provocative, you will be asked to cover up. Collars and leashes are fine, but overt bondage related behavior is not acceptable in public; so no dragging people around on leashes, please. Additionally, clothing and accessories that intentionally cause distress to other attendees are not welcome. If you’re wearing it, you will be asked to leave.

#### WPAFW expects attendees who consume beer at the event to do so responsibly.

In accordance with PA state law, if you appear to be visibly intoxicated, our bar wenches WILL cut you off. This decision cannot be appealed to another staff member.

#### Costume is not Consent.

Consent is necessary for all contact between attendees. No one has a right to touch anyone else, hugging or otherwise, without their permission, regardless of whether or not they’re in fursuit.

#### Manick is the mediator between attendees and the community.

If you have a problem with something involving operations, a WPAFW staff member, or another attendee, you must address it to Manick and not to local authorities. WPAFW will involve the local authorities if it becomes necessary.

## WPAFW Refund Policy

While we hope that everyone that registers for WPAFW can attend, we understand that situations arise in which it becomes infeasible. Due to this, WPAFW has created the following policies in regards to refunds, including when we can and cannot issue them:

* For the sake of this policy, Sponsorship will be defined as any level of purchase for the event.
* There is a refund deadline every year for Sponsorships which is the last day of Pre-registration`*`. For 2022, that date is **SEPTEMBER 21, 2022, at 11:59 pm EST**. Refund requests after this time will not be granted. Sponsorships cannot be transferred after the deadline.
* In order to obtain a refund, please send an email to registration@wpafw.org. We will need your LEGAL NAME (first and last) and BADGE NAME in order to properly identify you for a refund. NO other method of communication will serve as official in regards to refund requests (i.e. no Twitter, Telegram, etc.).
* Sponsorships will be refunded back to the credit card used in the purchase.
* You may request a transfer of Sponsorship before the refund deadline. We will need the following account information about the transferee: Legal name (first and last), Badge Name, and Date of Birth. Once transferred, the Sponsorship is the property of the person whose name it is in now.
* Sponsorships may be downgraded or upgraded before Pre-registration is closed. In the case of downgrading, a refund will be needed first. It will be open again on-site to upgrade to Sponsor only.

IN THE CASE OF WPAFW BEING CANCELLED: 

* Sponsorships will be refunded back to the purchase point.

`*` For Super-Sponsors this refund deadline is the date of closing for Super-Sponsor registration! For 2022, that date is **AUGUST 22, 2022, 11:59 pm EST**.

## Keep up to date with updates here or the following places:

[<span class="fa-stack fa-1x registration-icons">
<i class="fas fa-circle fa-stack-2x"></i>
<i class="fab fa-telegram-plane fa-stack-1x fa-inverse"></i>
</span> Telegram Chat](https://t.me/wpafw)

[<span class="fa-stack fa-1x registration-icons">
<i class="fas fa-circle fa-stack-2x"></i>
<i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
</span> Twitter](https://twitter.com/wpafw)

[<span class="fa-stack fa-1x registration-icons">
<i class="fas fa-circle fa-stack-2x"></i>
<i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
</span> Facebook](https://www.facebook.com/wpafw)
