---
title: Charity | WPAFW
permalink: "/charity"
layout: page
banner: 🕊️ Charity
banner-color: purple
nav: true
nav-title: Charity
nav-order: 5
---

# Charities

Every year, WPAFW chooses a local animal rescue/rehabilitation charity to support with our famed charity raffle. Previous charities include [Going Home Greyhounds, Inc](https://www.goinghomegreyhounds.org/) in Wexford, [Hide-E-Hole Ferret Rescue](https://hide-e-hole.com/) in Mt. Oliver, [Clarion PAWS](http://www.clarionpaws.org/) in Clarion, and [White Oak Animal Safe Haven](https://whiteoakanimalsafehaven.com/) in White Oak, and [The Awesome Spirit of Wildlife](https://tasow.org).

WPAFW 2022's charity is still being decided upon. In 2021, however, we had 392 attendees raise **$13,358.75** for [TASOW](https://tasow.org)! That's over $34 per person! Counting the first raffle fundraiser in 2005, this event has raised over $70,000 for charity! 

Keep an eye out on [our social media channels]({{'/contact#social-media' | absolute_url}}) for more information.

See you there!
