---
title: Featured Artists | WPAFW
permalink: "/artists"
layout: page
banner: 👨‍🎨️ Featured Artists
banner-color: blue
nav: true
nav-title: Artists
nav-order: 7
---

# Artists (2021)

## 🎨 Featured Artist(s)


<div class="columns is-mobile">
<div class="column is-half is-offset-one-quarter">
<div class="card">
<div class="card-image">
<figure class="image is-square">
<img src="{{'/assets/img/artists/lilshark.jpg' | absolute_url}}" alt="LilShark's Avatar.">
</figure>
</div>
<div class="card-content">
<div class="media">
<div class="media-content">
<p class="title is-4">LilShark</p>
<p class="subtitle is-6">[@atinylilshark](https://twitter.com/atinylilshark)</p>
</div>
</div>

<div class="content">
<p> LilShark is an artist located out of central Ohio. They began in the fandom about 8 years ago now, after attending and participating in the anime and cosplay community. A friend took a hard notice in their love of creating and drawing monsters and the rest is history!</p>

<p>Their current art focus is on the more adult side of furry but in the end their enjoyment spans not just adult art, but safe for work art and fursuiting. They hope to continue improving their craft and finding fun projects to work on in the future.</p>
</div>
</div>
</div>
</div>
</div>



## 🎧 Featured Musicians

in no particular order: 

* **Alphashock**
  - [facebook](https://www.facebook.com/Alphashock-109154567109659/) / [twitter](https://twitter.com/Alphashock_8)
* **Blaze Shepherd**
  - [twitter](https://twitter.com/blushepherd)
* **n00neimp0rtant**
  - [soundcloud](https://soundcloud.com/n00neimp0rtant-1) / [mixcloud](https://www.mixcloud.com/n00neimp0rtant/) / [twitter](https://twitter.com/n00neimp0rtant/)
* **DJ Workaholic**
  -  [facebook](http://Facebook.com/workaholcRaves) / [twitter](http://Twitter.com/HitchTheFox) / [instagram](http://Instagram.com/dj_workaholic) / [soundcloud](http://Soundcloud.com/dj-workaholic)
* **IPON3**
  - [soundcloud](https://soundcloud.com/dajuanza)
* **Pavlov - Pav-E** 


# Previously featured artists

- Story ([@story_trail](https://twitter.com/story_trail))
- Short ([@ShortCircuitBat](https://twitter.com/ShortCircuitBat))
- Ryuko ([@RyukoYuzuki](https://twitter.com/RyukoYuzuki))
- Vix ([@VixNdwnq](https://twitter.com/VixNdwnq))
- Sylph ([@tidehardt](https://twitter.com/tidehardt))
- Seb ([@Seb_silverfox](https://twitter.com/Seb_silverfox/))
- ...and many more!
