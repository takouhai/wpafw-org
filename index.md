---
title: Home | WPAFW
permalink: "/"
layout: home
banner: Home
banner-image: hero.jpg
nav: true
nav-title: Home
nav-order: 0
---

<section class="section">


# WPAFW 2022

Hello everyfur! WPAFW would like to extend our thanks and gratitude for the generous donations made last year! Here is some information about registration to get you ready for WPAFW 2022:

- Registration opens **June 20, 2022**
- SuperSponsors registration will close on **August 20, 2022**
- Regular registration will close on **September 21, 2022**
- SuperSponsors will be limited to **80 people** this year! Register early to make sure you get your spot in the event with the GOH!
- First 200 registrants will receive a special gift!
- We are capping registration at **500 people** this year, so be sure to pre-register to hold your place!

WPAFW 2022 will be on **October 7-9, 2022**! We can't wait to see you there!

<div class="buttons">
  [<button class="button is-link">Registration Information</button>](registration)
</div>


---



## WPAFW COVID-19 Information

For information on our COVID-19 Policies for WPAFW 2021, please visit the following link: 

<div class="buttons">
  [<button class="button is-link">COVID-19 Policies</button>](covid)
</div>

---
</section>

<section class="section">

### Results from WPAFW 2021!


WPAFW 2022's charity is still being decided upon. In 2021, however, we had 392 attendees raise **$13,358.75** for [TASOW](https://tasow.org)! That's over $34 per person! Counting the first raffle fundraiser in 2005, this event has raised over ***$70,000*** for charity! 

</section>

<section class="section">

## Contact us

> **Email:** [event@wpafw.org](mailto:event@wpafw.org)
>
> [**North Park Lodge**](https://goo.gl/maps/o1S7uUwtQZ2aN6wi9)
>
> North Ridge Drive
>
> Allison Park, PA 15101


</section>
