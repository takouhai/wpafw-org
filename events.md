---
title: Events | WPAFW
permalink: "/events"
banner: 🗓️ Events
banner-color: orange
nav: true
nav-title: Events
nav-order: 3
layout: page
---

# Events (2022)

Check out [our schedule for last year]({{'/assets/WPAFW2021EventSchedule.pdf' | absolute_url}}) to see what shenanigans we usually get up to:

<div class="columns is-mobile is-centered">
<div class="column is-three-quarters">
<embed id="content" src="{{'/assets/WPAFW2021EventSchedule.pdf' | absolute_url}}" width="100%"/>
</div>
</div>

Schedule changes and updates will be posted to the WPAFW Events Telegram Channel @ [t.me/wpafwofficial](https://t.me/wpafwofficial).

We hope to see you there!
